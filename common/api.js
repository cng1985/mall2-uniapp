let baseUrl = "";
let fileUrl="";
let shop=10;
const env = "dev";
if (env === "dev") {
	baseUrl = 'http://192.168.1.188:8001';
	fileUrl = "http://192.168.1.188:8001/oss/upload";
} else if (env === "pro") {
	baseUrl = 'https://api.nbsaas.com/mall2Front';
	fileUrl = "https://api.nbsaas.com/mall2Front/oss/upload";
}


const uploadFile = (filePath) => {
	return new Promise((resolve, reject) => {
        let header={};
		header["Authorization"] = uni.getStorageSync("token");

		uni.uploadFile({
			url: fileUrl, //仅为示例，非真实的接口地址
			filePath: filePath,
			name: 'file',
			formData: {
				'user': 'test'
			},
			header: header,
			success: (res) => {
				resolve(JSON.parse(res.data))
			},
			fail: (err) => {
				let result = {
					code: 500,
					msg: "获取数据失败"
				};
				reject(result)
			}
		});


	})
}
const request = (url, method = 'GET', data = {}, header = {}) => {
	return new Promise((resolve, reject) => {

		header["Authorization"] = uni.getStorageSync("token");
		data.shop=shop;
		uni.request({
			url: baseUrl + url,
			method: method,
			data: data,
			header: header,
			success: (res) => {
				if (res.data.code == 401) {
					uni.reLaunch({
						url: "/pages/login/index"
					})
				}
				resolve(res.data)
			},
			fail: (err) => {
				let result = {
					code: 500,
					msg: "获取数据失败"
				};
				reject(result)
			}
		})
	})
}

export const login=()=>{
	return new Promise((resolve, reject) => {
		uni.login({
			provider: 'weixin',
			success: async (wxRes) => {
		           resolve(wxRes);
			},
			fail: () => {
				reject("获取信息失败");
			}
		});
	})
}
export const form = (url, param) => {
	return request(url, "post", param, {
		'Content-Type': 'application/x-www-form-urlencoded'
	})
}
export const post = (url, param) => {
	return request(url, "post", param, {
		'Content-Type': 'application/json'
	})
}
const loadPostData = (url, param, ref) => {
	let res = post(url, param);
	res.then((res) => {
		if (res.code !== 200) {
			return;
		}
		ref.value = res.data || [];
	}).catch((err) => {
		console.log(err);
	})

}
const loadFormData = (url, param, ref) => {
	let res = form(url, param);
	res.then((res) => {
		if (res.code !== 200) {
			return;
		}
		ref.value = res.data || [];
	}).catch((err) => {
		console.log(err);
	})

}
const loadPostCallback = (url, param, callback) => {
	let res = post(url, param);
	res.then((res) => {
		if (callback) {
			callback(res);
		}
	}).catch((err) => {
		console.log(err);
	})

}
export default {
	request,
	form,
	post,
	loadPostData,
	loadFormData,
	loadPostCallback,
	uploadFile,
	login
};
export {uploadFile};
