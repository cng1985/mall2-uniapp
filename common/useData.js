import {
	ref
} from "vue";
import {
	post
} from "/common/api.js"
import {
	onShow
} from "@dcloudio/uni-app";

export function useData(functionMethod, searchObject) {
	//集合
	let modelData = ref({})


	onShow(async () => {
		let res = await post(functionMethod, searchObject.value);
		if (res.code !== 200) {
			return;
		}
		modelData.value = res.data;
	})
	return {
		modelData
	}
}