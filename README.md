# mall2-uniapp

#### 介绍
mall2第二代电商平台，一中台，多平台，多应用，对内快速构建应用，对外快速构建平台。支持b2c、b2b模式。包括平台端，代理商端，商家端，小程序，平台小程序，商家小程序。

#### 软件架构
mall2-uniapp


#### 软件主要功能

- 首页
  - 动态首页
  - 搜索
- 分类
  - 分类列表
    - 商品详情
    - 下单
- 购物车
- 我的
  - 订单
  - 用户地址
  - 用户积分
  - 用户钱包
  - 优惠券
  - 浏览记录
  - 个人资料
 

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request



#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
